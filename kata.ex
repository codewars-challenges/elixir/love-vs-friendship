defmodule Kata do
  def words_to_marks(s) do
    String.graphemes(s)
    |> Stream.map(fn c -> (c |> String.to_charlist |> hd) - ('a'|> hd) + 1 end)
    |> Enum.sum
  end
end

words_to_marks = &Kata.words_to_marks/1
IO.puts(words_to_marks.("atest"))
IO.puts(words_to_marks.("love"))
IO.puts(words_to_marks.("friendship"))